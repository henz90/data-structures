package day02sortpeople;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Day02Sortpeople {

    static class Person implements Comparable<Person>{

        String name;
        int age;
        double heightMeters;
        //  CONSTRUCTOR
        public Person(String name, int age, double height) {
            this.age = age;
            this.heightMeters = height;
            this.name = name;
        }
        
        public static final Comparator<Person> ageComparator = (Person p1, Person p2) -> p1.age - p2.age;
        
        /*
        public static final Comparator<Person> ageComparator = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.age - p2.age;
            }
        };
        */
        
        public static final Comparator<Person> heightComparator = (Person p1, Person p2) -> {
            return Double.compare(p1.heightMeters, p2.heightMeters);
            /*
            if (p1.heightMeters < p2.heightMeters) return -1;
            if (p1.heightMeters > p2.heightMeters) return 1;
            return 0;
            */
        };
        
        /*
        public static final Comparator<Person> heightComparator = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p1.heightMeters < p2.heightMeters) {
                return -1;
            }
            if (p1.heightMeters > p2.heightMeters) {
                return 1;
            }
            return 0;
            }
        };
        */
        
        @Override
        public String toString() {
            return "Name=" + name + ", Age=" + age + ", Height=" + heightMeters + "m";
        }

        @Override
        public int compareTo(Person o) {
            return name.compareTo(o.name);
        }
    }

    public static void main(String[] args) {
        //  Initialize People
        ArrayList<Person> personList = new ArrayList<>();
        personList.add(new Person("Henry", 29, 1.25));
        personList.add(new Person("Aaron", 13, 1.400));
        personList.add(new Person("Stacy", 42, 1.55));
        personList.add(new Person("Marvin", 23, 1.27));
        personList.add(new Person("Ray", 4, 0.88));
        System.out.println("Person List:");
        for (Person p : personList) {
            System.out.println(p);
        }
        System.out.println("");
        
        //  Sort by Name
        Collections.sort(personList);
        System.out.println("Person List Sorted by Name:");
        for (Person p : personList) {
            System.out.println(p);
        }
        System.out.println("");
        
        //  Sort by Age
        Collections.sort(personList, Person.ageComparator);
        System.out.println("Person List Sorted by Age:");
        for (Person p : personList) {
            System.out.println(p);
        }
        System.out.println("");
        
        //  Sort by Age LAMBDA
        Collections.sort(personList, (Person p1, Person p2) -> p1.age - p2.age);
        System.out.println("Person List Sorted by Age (LAMBDA):");
        for (Person p : personList) {
            System.out.println(p);
        }
        System.out.println("");
        
        //  Sort by Height
        Collections.sort(personList, Person.heightComparator);
        System.out.println("Person List Sorted by Height:");
        for (Person p : personList) {
            System.out.println(p);
        }
        System.out.println("");
    }
}
