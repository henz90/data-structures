package finaltreepatterns;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class TreeStringIntSet implements Iterable<Pair<String, Integer>> {

    @Override
    public Iterator<Pair<String, Integer>> iterator() {
        return (Iterator<Pair<String, Integer>>) new TreeStringIntSet.Node().valuesSet;
    }

    class Node {

        Node left, right;
        String key; // keys are unique
        // HashSet is like ArrayList except it does not hold duplicates
        HashSet<Integer> valuesSet = new HashSet<>(); // unique only
    }

    private Node root;

    // throws DuplicateValueException if this key already contains such value
    void add(String key, int value) throws DuplicateValueException {
        Node newNode = new Node();
        newNode.key = key;
        newNode.valuesSet.add(value);
        if (root == null) {
            root = newNode;
            return;
        }
        Node currentNode = root;
        while (currentNode != null) {
            if (currentNode.key.equals(key)) {
                if (currentNode.valuesSet.contains(value)) {
                    throw new DuplicateValueException();
                } else {
                    currentNode.valuesSet.add(value);
                    return;
                }
            }
            if (currentNode.left == null) {
                currentNode.left = newNode;
                return;
            } else {
                currentNode = currentNode.left;
            }
            if (currentNode.right == null) {
                currentNode.right = newNode;
                return;
            } else {
                currentNode = currentNode.right;
            }
        }
    }

    boolean containsKey(String key) {
        Node currentNode = root;
        while (true) {
            if (currentNode.key.equals(key)) {
                return true;
            } else if (currentNode.left != null) {
                currentNode = currentNode.left;
            } else if (currentNode.right != null) {
                currentNode = currentNode.right;
            } else {
                return false;
            }
        }
    }

    ArrayList<Integer> getValuesByKey(String key) {
        ArrayList<Integer> valuesList = new ArrayList<>();
        Node currentNode = root;
        while (currentNode != null) {
            if (currentNode.key.equals(key)) {
                for (int v : currentNode.valuesSet) {
                    valuesList.add(v);
                }
                return valuesList;
            }
            if (currentNode.left == null) {
                currentNode = currentNode.right;
            }
            if (currentNode.right == null) {
                currentNode = currentNode.left;
            }
        }
        return valuesList;
    } // return empty list if key not found

    ArrayList<String> getKeysContainingValue(int value) {
        ArrayList<String> keysList = new ArrayList<>();
        Node currentNode = root;
        while (currentNode != null) {
            if (currentNode.valuesSet.contains(value)) {
                keysList.add(currentNode.key);
            }
            if (currentNode.left == null) {
                currentNode = currentNode.right;
            }
            if (currentNode.right == null) {
                currentNode = currentNode.left;
            }
        }
        return keysList;
    }

    ArrayList<String> getAllKeys() {
        ArrayList<String> keysList = new ArrayList<>();
        Node currentNode = root;
        while (currentNode != null) {
            keysList.add(currentNode.key);
            if (currentNode.left == null) {
                currentNode = currentNode.right;
            }
            if (currentNode.right == null) {
                currentNode = currentNode.left;
            }
        }
        return keysList;
    }

    // add code for observer that is called on each node's two operations:
    // add succeeded or add failed.
    // It will take three parameters:
    // key, value being added, string describing operation ("Add" or "Add-Failed")
    public interface TreeEventObserverInt {
        public void event(String key, int value, String operation);
    }

    // You can add toString() for debugging if you like
    // You can add other private methods and fields as needed. But no public ones.
}
