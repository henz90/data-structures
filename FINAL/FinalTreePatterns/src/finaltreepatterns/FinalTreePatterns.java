package finaltreepatterns;

import java.util.ArrayList;
import java.util.Iterator;

class DuplicateValueException extends Exception {
}

public class FinalTreePatterns {

    public static void main(String[] args) throws DuplicateValueException {
        TreeStringIntSet set = new TreeStringIntSet();
        set.add("Tom", 0);
        set.add("Tom", 1);
        set.add("Tom", 2);
        set.add("Tom", 3);
        set.add("Tom", 4);
        set.add("Tom", 5);
        set.add("Tim", 0);
        set.add("Tim", 1);
        set.add("Tam", 0);
        set.add("Tam", 1);
        set.add("Tum", 0);
        set.add("Tum", 1);
        ArrayList<String> allKeys = set.getAllKeys();
        System.out.println(allKeys);
    }

}
