package finaltreepatterns;

import org.junit.Test;
import static org.junit.Assert.*;

public class TreeStringIntSetTest {

    public TreeStringIntSetTest() {
    }

    /**
     * Test of add method, of class TreeStringIntSet.
     * @throws finaltreepatterns.DuplicateValueException
     */
    @Test
    public void testAdd() throws DuplicateValueException {
        System.out.println("add");
        TreeStringIntSet set = new TreeStringIntSet();
        set.add("Tom", 0);
        set.add("Tim", 0);
        set.add("Tam", 0);
        set.add("Tum", 0);
        assertEquals("[Tom, Tim, Tam, Tum]",set.getAllKeys().toString());
    }

    /**
     * Test of containsKey method, of class TreeStringIntSet.
     * @throws finaltreepatterns.DuplicateValueException
     */
    @Test
    public void testContainsKey() throws DuplicateValueException {
        System.out.println("containsKey");
        TreeStringIntSet instance = new TreeStringIntSet();
        instance.add("Tom", 0);
        boolean expResult = true;
        boolean result = instance.containsKey("Tom");
        assertEquals(expResult, result);
    }

    /**
     * Test of getValuesByKey method, of class TreeStringIntSet.
     * @throws finaltreepatterns.DuplicateValueException
     */
    @Test
    public void testGetValuesByKey() throws DuplicateValueException {
        System.out.println("getValuesByKey");
        TreeStringIntSet set = new TreeStringIntSet();
        set.add("Tom", 0);
        set.add("Tom", 1);
        set.add("Tom", 2);
        set.add("Tom", 3);
        set.add("Tom", 4);
        set.add("Tom", 5);
        assertEquals("[0, 1, 2, 3, 4, 5]", set.getValuesByKey("Tom").toString());
    }

    /**
     * Test of getKeysContainingValue method, of class TreeStringIntSet.
     * @throws finaltreepatterns.DuplicateValueException
     */
    @Test
    public void testGetKeysContainingValue() throws DuplicateValueException {
        System.out.println("getKeysContainingValue");
        TreeStringIntSet set = new TreeStringIntSet();
        set.add("Tom", 1);
        set.add("Tom", 2);
        set.add("Tom", 3);
        set.add("Tom", 4);
        set.add("Tom", 5);
        set.add("Tim", 0);
        set.add("Tim", 1);
        set.add("Tam", 0);
        set.add("Tam", 1);
        set.add("Tum", 0);
        set.add("Tum", 1);
        assertEquals("[Tim, Tam, Tum]",set.getKeysContainingValue(0).toString());
        
    }

    /**
     * Test of getAllKeys method, of class TreeStringIntSet.
     * @throws finaltreepatterns.DuplicateValueException
     */
    @Test
    public void testGetAllKeys() throws DuplicateValueException {
        System.out.println("getAllKeys");
        TreeStringIntSet set = new TreeStringIntSet();
        set.add("Tom", 0);
        set.add("Tim", 0);
        set.add("Tim", 1);
        set.add("Tam", 0);
        set.add("Tam", 1);
        set.add("Tum", 0);
        set.add("Tum", 1);
        assertEquals("[Tom, Tim, Tam, Tum]", set.getAllKeys().toString());
    }

}
