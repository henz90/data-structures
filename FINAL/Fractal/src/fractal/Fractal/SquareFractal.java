/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal.Fractal;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author HP-ENVY
 */
public class SquareFractal extends javax.swing.JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        int x = 0;
        int y = 0;
        int width = getWidth();
        int height = getHeight();
        DrawFractal(g2d, x, y, width, height, 4);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }

    private void DrawFractal(Graphics2D g2d, int x, int y, int width, int height, int level) {
        if (level <= 4) {
            g2d.drawRect(x, y, width, height - 1);
            
            int w2 = width / 2;
            int h2 = height / 2;
            g2d.drawLine(x, w2, h2, 0);
            g2d.drawLine(width, w2, h2, height);
            g2d.drawLine(width / 2, y, height, h2);
            g2d.drawLine(width / 2, width, 0, h2);
            
            int w4 = width / 4;
            int h4 = height /4;
            g2d.drawRect(x, y, w4, h4);
            g2d.drawRect(width - w4, height - h4, w4, h4);

            g2d.drawRect(width - w4, y, w4, h4);
            g2d.drawRect(x, height - h4, w4, h4);
        }
    }

    public SquareFractal() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
