package day02arraysearch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Day02ArraySearch {

    static Scanner input = new Scanner(System.in);

    static int inputInt() {
        while (true) {
            try {
                int value = input.nextInt();
                return value;
            } catch (InputMismatchException ex) {
                input.nextLine();
                System.out.println("Invalid input, try again.");
            } finally {
                input.nextLine();
            }
        }
    }

    // If exists, return the element, otherwise return 0
    static int getIfExists(int[][] data, int row, int col) {
        if (data != null) {
            return data[row][col];
        }
        return 0;
    }

    // return sum of the element at row/col
    static int sumOfCross(int[][] data, int row, int col) {
        // plus (if they exist) element above, below, to the left and right of it
        if (row >= data.length || col >= data[0].length) {
            System.out.println("Error: Values entered are greater than array size: " + data.length + "x" + data[0].length);
            return 0;
        }
        int sum = data[row][col];
        if (row - 1 >= 0) {
            sum += getIfExists(data, row - 1, col); //top
        }
        if (row + 1 < data.length) {
            sum += getIfExists(data, row + 1, col); //bottom
        }
        if (col - 1 >= 0) {
            sum += getIfExists(data, row, col - 1); //left
        }
        if (col + 1 < data[0].length) {
            sum += getIfExists(data, row, col + 1); //right
        }
        return sum;
    }

    //search that will find which element at row/col has the smallest sum of itself and elements surrounding it.
    static void findSmallest(int[][] data) {
        int min = 100;
        int row = 0;
        int col = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[0].length; j++) {
                if (sumOfCross(data, i, j) < min) {
                    min = sumOfCross(data, i, j);
                    row = i;
                    col = j;
                }
            }
        }
        System.out.println("MIN:" + min + " | Found on row: " + row + " column: " + col);
    }

    static void print2D(int[][] data) {
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                System.out.print(data[row][col] + (col == data[row].length - 1 ? "" : ", "));
            }
            System.out.println("");
        }
        System.out.println("");
    }

    static int[][] duplicateArray2D(int[][] orig2d) {
        int[][] sumArray = new int[orig2d.length][orig2d[0].length];
        for (int row = 0; row < orig2d.length; row++) {
            for (int col = 0; col < orig2d[0].length; col++) {
                sumArray[row][col] = sumOfCross(orig2d, row, col);
            }
        }
        print2D(sumArray);
        return sumArray;
    }

    public static void main(String[] args) {
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},};
        while (true) {
            System.out.println("Choose One:\n"
                    + "1. Compute Sum of Cross\n"
                    + "2. Find the Smallest Sum of Cross\n"
                    + "3. Sum of Cross as new Array\n"
                    + "0. Exit.");
            int choice = inputInt();
            switch (choice) {
                case 0:
                    System.out.println("Goodbye...");
                    System.exit(0);
                    break;
                case 1:
                    System.out.print("Choose a row: ");
                    int row = inputInt();
                    System.out.print("Choose a column: ");
                    int col = inputInt();
                    System.out.println("Sum of Cross: " + sumOfCross(data2D, row, col));
                    break;
                case 2:
                    findSmallest(data2D);
                    break;
                case 3:
                    System.out.println("ORIGINAL ARRAY:");
                    print2D(data2D);
                    System.out.println("");
                    System.out.println("NEW ARRAY:");
                    duplicateArray2D(data2D);
                    break;
            }
        }
    }
}
