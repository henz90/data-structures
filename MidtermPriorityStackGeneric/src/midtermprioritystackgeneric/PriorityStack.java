package midtermprioritystackgeneric;

// Your task is to complete the implementation of the class below.
import java.util.NoSuchElementException;

// You are not allowed to use complex data structures such as ArrayList, HashMap, etc.
// You are also not allowed to use Collections and Arrays *classes*.
// You are allowed to use arrays, of course.
// When in doubt - it is your responsibility ask the teacher DURING the midterm, not after.
// WARNING: Your solution will be uploaded to a plagiarism-checking system.
// Only submit your own original work.
public class PriorityStack<T> {

    private class Container<T> {

        T value;
        boolean hasPriority;
        Container<T> nextBelow;
    }

    private Container<T> top; // top of the stack element

    private int size = 0;

    public void push(T value) {
        push(value, false);
    }

    public void push(T value, boolean hasPriority) {
        Container<T> newCont = new Container<T>();
        newCont.hasPriority = hasPriority;
        newCont.value = value;
        if (size == 0) {
            top = newCont;
            size++;
        } else {
            Container<T> lastTop = top;
            newCont.nextBelow = lastTop;
            top = newCont;
            size++;
        }
    }

    public T pop() {
        // remove and return the top item
        // if no item found (size == 0) then throw NoSuchElementException
        if (size == 0) {
            throw new NoSuchElementException();
        } else {
            Container<T> toDelete = top;
            top = top.nextBelow;
            size--;
            return toDelete.value;
        }
    }

    public T popPriority() {
        // find item with priority starting from the top, remove it and return it
        // if no priority item found then remove and return the top item
        // if stack is empty then throw NoSuchElementException
        if (size == 0) {
            throw new NoSuchElementException();
        }
        if (top.hasPriority == true) {
            Container<T> toDelete = top;
            pop();
            return toDelete.value;
        }
        for (Container<T> current = top; current != null; current = current.nextBelow) {
            if (current.nextBelow.hasPriority == true && current.nextBelow != null) {
                Container<T> toDelete = current.nextBelow;
                current.nextBelow = current.nextBelow.nextBelow;
                size--;
                return toDelete.value;
            } else {
                Container<T> toDelete = top;
                pop();
                return toDelete.value;
            }
        }
        throw new RuntimeException("Internal error: invalid control flow");
    }

    public int hasValue(T value) {
        // returns -1 if value is not on the stack
        // this code only looks for the *first* occurence of the value, starting from top
        // WARNING: you must call value.equals(item.value) to determine whether
        // two values are equal, just like you would do for a String
        // returning value 0 means the value is on top of the stack,
        // 1 means 1 below the top, and so on...
        int count = 0;
        for (Container<T> current = top; current != null; current = current.nextBelow, count++) {
            if (current.value.equals(value)) {
                return count;
            }
        }
        return -1;
    }

    public T removeValue(T value) {
        // removes the first item from top containing the value and returns the value
        // if item with value is not found throw NoSuchElementException
        int check = hasValue(value);
        if (check == 0) {
            Container<T> toDelete = top;
            pop();
            return toDelete.value;
        }
        if (check > 0) {
            for (Container<T> current = top; current != null; current = current.nextBelow) {
                if (current.nextBelow.value.equals(value)) {
                    Container<T> toDelete = current.nextBelow;
                    current.nextBelow = current.nextBelow.nextBelow;
                    size--;
                    return toDelete.value;
                }
            }
        }
        throw new NoSuchElementException();
    }

    public int getSize() {
        return size;
    }

    public void reorderByPriority() {   //  ????
        // reorder items (re-create a new stack, if you like)
        // where all priority items are on top and non-priority items are below them
        // Note: order within the priority items group and non-priority items group must remain the same
        // Suggestion: instead of reordering the existing stack items
        // it may be easier to re-create a new stack with items in the order you need
        int pCount = 0;
        int nCount = 0;
        for (Container<T> current = top; current != null; current = current.nextBelow) {
            if (current.hasPriority == true) {
                pCount++;
            } else {
                nCount++;
            }
        }
        Object[] priorityList = new Object[pCount];
        Object[] normalList = new Object[nCount];
        for (int i = 0; i < pCount; i++) {
            for (Container<T> current = top; current != null; current = current.nextBelow) {
                if (current.hasPriority == true) {
                    priorityList[i] = current.value;
                    i++;
                }
            }
        }
        for (int i = 0; i < nCount; i++) {
            for (Container<T> current = top; current != null; current = current.nextBelow) {
                if (current.hasPriority == false) {
                    normalList[i] = current.value;
                    i++;
                }
            }
        }
        top = null;
        size = 0;
        for (int i = normalList.length - 1; i >= 0; i--) {
            push((T) normalList[i]);
        }
        for (int i = priorityList.length - 1; i >= 0; i--) {
            push((T) priorityList[i], true);
        }
        //throw new UnsupportedOperationException("Prototype - not implemented yet");
    }

    @Override
    public String toString() {
        // return string describing the contents of the stack, starting from the top
        // Use value.toString() to conver values kept in the stack to strings.
        // Format exactly like this (assuming T is a string to keep it simple):
        // "[Jerry:N,Terry:N,Martha:P,Tom:P,Jimmy:N]" 
        // N means item has no priority, P means item has priority
        // For full marks you must use StringBuilder, no + (string concatenation) allowed.
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Container current = top; current != null; current = current.nextBelow) {
            sb.append(current == top ? "" : ","); //  Comma Sepperated
            sb.append(current.value.toString());
            sb.append(current.hasPriority == true ? ":P" : ":N");
        }
        sb.append("]");
        return sb.toString();
    }

    public T[] toArrayReversed() { // Note: this is "the twist"
        // return array with items on the stack
        // WARNING: element 0 of the array must be the BOTTOM of the stack
        // NOTE: To obtain full marks for this method you must use recursion.
        // Collect items on your way back, just before returning.
        // This case is similar to when constructors of parent classes are called (Programming II course).
        T[] itemList =  (T[]) new Object[size];
        for (int i = 0; i < size; i++) {
            for (Container<T> current = top; current != null; current = current.nextBelow) {
                itemList[i] = current.value;
                i++;
            }
        }
        T[] reverseList = (T[]) reverse(itemList,size);
        return reverseList;
    }
    
    private static Object[] reverse(Object a[], int n) 
    { 
        Object[] b = new Object[n]; 
        int j = n; 
        for (int i = 0; i < n; i++) { 
            b[j - 1] = a[i]; 
            j = j - 1; 
        }
        return b;
    } 
    // NOTE: you are only allowed to add private methods and private fields (if needed)
}
