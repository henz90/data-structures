package day01universalarrays;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Day01UniversalArrays {

    static Scanner input = new Scanner(System.in);

    static int[] static1dArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    static int[][] static2dArray = {{10, 20, 30, 40, 50}, {60, 70, 80, 90, 100}};

    static int inputInt() {
        while (true) {
            try {
                int value = input.nextInt();
                return value;
            } catch (InputMismatchException ex) {
                input.nextLine();
                System.out.println("Invalid input, try again.");
            } finally {
                input.nextLine();
            }
        }
    }

    static int random(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt(max - min + 1) + min;
        return randomNum;
    }

    static void inputArray(int[] data) {
        System.out.println("Array size is: " + data.length);
        for (int i = 0; i < data.length; i++) {
            System.out.print("Enter value for Row " + (i + 1) + ": ");
            data[i] = inputInt();
        }
    }

    static void inputArray(int[][] data2d) {
        System.out.println("Array size is: " + data2d[1].length + "x" + data2d.length);
        for (int i = 0; i < data2d.length; i++) {
            for (int j = 0; j < data2d[i].length; j++) {
                System.out.print("Enter value for Row " + (i + 1) + " Column " + (j + 1) + ": ");
                data2d[i][j] = inputInt();
            }
        }
    }

    static void printArray(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.print((i == 0 ? "" : ",") + data[i]);
        }
        System.out.println("");
    }

    static void printArray(int[][] data2d) {    //Unsure how to print.
        for (int i = 0; i < data2d.length; i++) {
            for (int j = 0; j < data2d[i].length; j++) {
                System.out.print((i == 0 ? "" : ",") + data2d[i][j]);
            }
        }
        System.out.println("");
    }

    static int[] findDuplicates(int[] a1, int[] a2) {
        int count = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] == a2[j]) {
                    count++;
                }
            }
        }
        System.out.println(count + " duplicates found.");
        int[] duplicates = new int[count];
        for (int j = 0; j < a1.length; j++) {
            for (int k = 0; k < a2.length; k++) {
                if (a1[j] == a2[k]) {
                    for (int i = 0; i < duplicates.length; i++) {
                        duplicates[i] = a1[j];
                    }
                }
            }
        }
        printArray(duplicates);
        return duplicates;
    }

    /*
    static int[] findDuplicates(int [][] a1, int[][]a2) {
         
    }
     */
    
    static int[] join(int[] a1, int[] a2) { //  Unsure of question
        int size = a1.length + a2.length;
        int[] joinedArray = new int[size];
        System.out.println("Array size is " + size);
        return joinedArray;
    }

    public static void main(String[] args) {
        while (true) {
            System.out.println("Choose One:\n"
                    + "1. One Dimensional Arrays\n"
                    + "2. Two Dimensional Arrays\n"
                    + "0. EXIT");
            int choice = inputInt();
            switch (choice) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    int[] array1d = new int[random(2,10)];
                    while (true) {
                        System.out.println("Choose One:\n"
                                + "1. Input Data\n"
                                + "2. Print Data\n"
                                + "3. Find Duplicates\n"
                                + "4. Join Arrays\n"
                                + "0. EXIT");
                        int choice1d = inputInt();
                        switch (choice1d) {
                            case 0:
                                return;
                            case 1:
                                inputArray(array1d);
                                break;
                            case 2:
                                printArray(array1d);
                                break;
                            case 3:
                                findDuplicates(array1d, static1dArray);
                                break;
                            case 4:
                                join(array1d, static1dArray);
                                break;
                        }
                    }
                case 2:
                    int[][] array2d = new int[random(2,5)][random(2,5)];
                    while (true) {
                        System.out.println("Choose One:\n"
                                + "1. Input Data\n"
                                + "2. Print Data\n"
                                + "3. Find Duplicates\n"
                                + "0. EXIT");
                        int choice1d = inputInt();
                        switch (choice1d) {
                            case 0:
                                return;
                            case 1:
                                inputArray(array2d);
                                break;
                            case 2:
                                printArray(array2d);
                                break;
                            case 3:
                                //findDuplicates(array2d, static2dArray);
                                break;
                        }
                    }
            }
        }
    }
}
