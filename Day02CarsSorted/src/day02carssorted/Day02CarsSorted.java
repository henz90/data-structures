package day02carssorted;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class InvalidValueException extends Exception {

    InvalidValueException(String msg) {
        super(msg);
    }
}

public class Day02CarsSorted {

    static final class Car implements Comparable<Car> {

        String makeModel;
        double engineSizeL;
        int prodYear;

        public String getMakeModel() {
            return makeModel;
        }

        public void setMakeModel(String makeModel) {
            this.makeModel = makeModel;
        }

        public double getEngineSizeL() {
            return engineSizeL;
        }

        public void setEngineSizeL(double engineSizeL) {
            this.engineSizeL = engineSizeL;
        }

        public int getProdYear() {
            return prodYear;
        }

        public void setProdYear(int prodYear) {
            this.prodYear = prodYear;
        }

        public Car(String makeModel, double engineSizeL, int prodYear) {
            this.makeModel = makeModel;
            this.engineSizeL = engineSizeL;
            this.prodYear = prodYear;
        }

        @Override
        public String toString() {
            return "Car - " + "Make Model: " + makeModel + ", Engine Size (L): " + engineSizeL + ", Year: " + prodYear;
        }

        @Override
        public int compareTo(Car o) {
            return makeModel.compareTo(o.makeModel);
        }

        public static final Comparator<Car> makeModelComparator = (Car c1, Car c2) -> {
            return c1.compareTo(c2);
        };

        public static final Comparator<Car> engineComparator = (Car c1, Car c2) -> {
            return Double.compare(c1.engineSizeL, c2.engineSizeL);
        };

        public static final Comparator<Car> yearComparator = (Car c1, Car c2) -> c1.prodYear - c2.prodYear;
        
        Car(String dataLine) throws InvalidValueException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 3) {
                throw new InvalidValueException("Number of fields in line invalid.");
            }
            setMakeModel(data[0]);
            setEngineSizeL(Double.parseDouble(data[1]));
            setProdYear(Integer.parseInt(data[2]));
        } catch (InvalidValueException ex) {
            throw new InvalidValueException("Error parsing.");
        }
    }
    }

    static ArrayList<Car> parking = new ArrayList<>();

    static void readDataFromFile() {
        try (Scanner fileInput = new Scanner(new File("cars.txt"))) {
            while (fileInput.hasNextLine()) {
                String dataLine = fileInput.nextLine();
                try {
                    Car car = new Car(dataLine);
                    parking.add(car);
                } catch (InvalidValueException ex) {
                    System.out.println("Data error: " + ex.getMessage());
                }
            }
            fileInput.close();
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        /*
        parking.add(new Car("Mazda", 7.5, 2020));
        parking.add(new Car("Honda", 6.0, 2001));
        parking.add(new Car("Lexus", 7.5, 2020));
        parking.add(new Car("Mazda", 8.0, 2010));
        parking.add(new Car("Toyota", 6.5, 2010));
        parking.add(new Car("Mercedes", 12.5, 2020));
        */

        readDataFromFile();
        
        //  Print Parking
        parking.forEach(c -> {  //  Ask about this:
            System.out.println(c);
        });

        //  Sort and Print by Make Model
        Collections.sort(parking, Car.makeModelComparator);
        System.out.println("Parking sorted by Make Model:");
        for (Car c : parking) {
            System.out.println(c);
        }
        System.out.println("");

        //  Sort and Print by Engine Size
        Collections.sort(parking, Car.engineComparator);
        System.out.println("Parking sorted by Engine Size (L):");
        parking.forEach(c -> {
            System.out.println(c);
        });
        System.out.println("");

        //  Sort and Print by Year
        Collections.sort(parking, Car.yearComparator);
        System.out.println("Parking sorted by Year:");
        parking.forEach(c -> {
            System.out.println(c);
        });
        System.out.println("");

        System.out.println("Parking sorted by Year then Model:");
        parking.stream().sorted(Car.yearComparator.thenComparing(Car.makeModelComparator)).forEach(Car -> System.out.println(Car)); //  Is this even legal?
    }
}
