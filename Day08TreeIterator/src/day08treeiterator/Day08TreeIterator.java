package day08treeiterator;

import java.util.Iterator;

class SimpleBinaryTree implements Iterable<Integer>{
    
    class SimpleBinaryTreeIterator implements Iterator<Integer>{
        
        private int[] valuesInOrder;
        private int currentIndex;

        public SimpleBinaryTreeIterator(int[] valuesInOrder) {
            this.valuesInOrder = valuesInOrder;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < valuesInOrder.length;
        }

        @Override
        public Integer next() {
            int value = valuesInOrder[currentIndex];
            currentIndex++;
            return value;
            //  return valuesInOrder[currentIndex++];
        }
        
    }
    
    @Override
    public Iterator<Integer> iterator() {
        return new SimpleBinaryTreeIterator(getValuesInOrder());
    }

    private class NodeOfInt {

        NodeOfInt left, right;
        int value;
    }

    NodeOfInt root;
    private int nodeCount;

    //  Throws exception if put attempts to insert value that already exists (duplicate)
    void put(int value) throws IllegalArgumentException {
        NodeOfInt node = new NodeOfInt();
        node.value = value;
        if (root == null) {
            root = node;
            nodeCount++;
            return;
        }
        NodeOfInt currentNode = root;
        while (true) {
            if (currentNode.value == value) {   //  NOTE: if we would use .equals() for reference types
                throw new IllegalArgumentException("Duplicates are not allowed");
            }
            if (currentNode.value > value) {    //  Go Left
                if (currentNode.left == null) {
                    currentNode.left = node;
                    nodeCount++;
                    return;
                } else {    //  Keep going Left
                    currentNode = currentNode.left;
                    //  Continue the loop to another iteration
                }
            } else {    //  Go Right
                if (currentNode.right == null) {
                    currentNode.right = node;
                    nodeCount++;
                    return;
                } else {    //  Keep going Left
                    currentNode = currentNode.right;
                    //  Continue the loop to another iteration
                }
            }
        }
    }
    
    private int getSumOfThisAndSubNodes(NodeOfInt node){
        if (node == null) {
            return 0;
        }
        int result;
        result = node.value;
        result += getSumOfThisAndSubNodes(node.left);
        result += getSumOfThisAndSubNodes(node.right);
        return result;
    }

    public int getSumofAllValues() {
        return getSumOfThisAndSubNodes(root);
    }
    
    private void collectValuesInOrder(NodeOfInt node){
        if (node == null){
            return;
        } else {
            collectValuesInOrder(node.right);
            resultArray[resultIndex] = node.value;
            resultIndex++;
            collectValuesInOrder(node.left);
        }
    }
    
    private int[] resultArray;
    private int resultIndex;
    
    int[] getValuesInOrder() {  //  From largest to smallest
        resultArray = new int[nodeCount];
        resultIndex = 0;    //  Next node value goes into resultArray[resultIndex]
        collectValuesInOrder(root);
        return resultArray;
    }

}

public class Day08TreeIterator {

    public static void main(String[] args) {
        SimpleBinaryTree tree = new SimpleBinaryTree();
        for (int n : new int[]{10, 7, 12, 15, 8, 5, 3, 11}) {
            tree.put(n);
        }
        int sum = tree.getSumofAllValues();
        System.out.println("Sum is: " + sum);
        System.out.print("Values in order: ");
        int [] valuesInOrder = tree.getValuesInOrder();
        for (int n : valuesInOrder) {
            System.out.print(n+",");
        }
        System.out.println("");
    }
}
