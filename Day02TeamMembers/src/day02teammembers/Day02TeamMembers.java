package day02teammembers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

class InvalidValueException extends Exception {

    InvalidValueException(String msg) {
        super(msg);
    }
}

public class Day02TeamMembers {

    static HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();

    static void readDataFromFile() {    //  NEEDS WORK!
        try (Scanner fileInput = new Scanner(new File("teams.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                try {
                    try {
                        String[] team = line.split(":");
                        if (team.length != 2) {
                            throw new InvalidValueException("Error in Line: "+ line);
                        }
                        String teamName = team[0];
                        String players = team[1];
                        String[] playerName = players.split(",");
                        for (String name : playerName){
                            if (playersByTeams.containsKey(name)) {
                                ArrayList<String> teamsList = playersByTeams.get(name);
                                teamsList.add(teamName);
                            } else{
                                ArrayList<String> teamsList = new ArrayList<>();
                                teamsList.add(teamName);
                                playersByTeams.put(name, teamsList);
                            }
                        }
                    } catch (InvalidValueException ex) {
                        throw new InvalidValueException("Error parsing.");
                    }
                } catch (InvalidValueException ex) {
                    System.out.println("Data error: " + ex.getMessage());
                }
            }
            fileInput.close();
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        readDataFromFile();
        playersByTeams.entrySet().forEach((entry -> {
            System.out.println(entry.getKey() + " plays in the following teams: " + entry.getValue());
        }));
    }
}
