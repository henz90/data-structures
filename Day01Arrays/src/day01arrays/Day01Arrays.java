package day01arrays;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Day01Arrays {

    static Scanner input = new Scanner(System.in);

    static boolean isPrime(int inputNumber) {
        if (inputNumber <= 1) {
            return false;
        }
        boolean isItPrime = true;
        for (int i = 2; i <= inputNumber / 2; i++) {
            if ((inputNumber % i) == 0) {
                return false; // not a prime number
            }
        }
        return true; // not divisible therefore a prime number
    }

    static int inputInt() {
        while (true) {
            try {
                int value = input.nextInt();
                return value;
            } catch (InputMismatchException ex) {
                input.nextLine();
                System.out.println("Invalid input, run again.");
                System.exit(0);
            } finally {
                input.nextLine();
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Please enter a size for the array:");
        int arraySize = inputInt();
        if (arraySize < 1) {
            System.out.println("Error: Please enter a number greater than 1");
            System.exit(1);
        } else {
            int[] array = new int[arraySize];
            for (int i = 0; i < array.length; i++) {
                Random rand = new Random();
                int min = 1;
                int max = 100;
                int randomNum = rand.nextInt(max - min + 1) + min;
                array[i] = randomNum;
            }
            System.out.println("Numbers Generated:");
            for (int i = 0; i < array.length; i++) {
                System.out.print((i == 0 ? "" : ",") + array[i]);
            }
            System.out.println("");
            ArrayList<Integer> primesOnlyList = new ArrayList<>();
            for (int i = 0; i < array.length; i++) {
                if (isPrime(array[i])) {
                    primesOnlyList.add(array[i]);
                }
            }
            // display comma-separated from ArrayList
            System.out.println("Prime Numbers:");
            for (int i = 0; i < primesOnlyList.size(); i++) {
                System.out.print((i == 0 ? "" : ",") + primesOnlyList.get(i));
            }
            System.out.println();
            System.out.println("");
        }
    }
}
