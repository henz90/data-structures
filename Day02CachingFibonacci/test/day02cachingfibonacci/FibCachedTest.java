package day02cachingfibonacci;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FibCachedTest {
    
    public FibCachedTest() {
    }
    
    @Test
    public void testGetNthFib() {
        FibCached instance = new FibCached();
        assertEquals(5L, instance.getNthFib(5));
        assertEquals(1597L, instance.getNthFib(5));
    }
    
    @Test
    public void testFibToString(){
        FibCached instance = new FibCached();
        instance.getNthFib(5);
        instance.getNthFib(7);
        instance.getNthFib(11);
        assertEquals("[0,1,1,2,3,5,8,13,34,55,89]", instance.toString());
    }
    
    @Test
    public void testFibIndexInvalid(){
        Exception exception = assertThrows()
        FibCached instance = new FibCached();
        instance.getNthFib(-1);
    }
}
