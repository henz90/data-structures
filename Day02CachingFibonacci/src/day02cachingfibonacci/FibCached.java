package day02cachingfibonacci;

import java.util.ArrayList;

class FibCached {

        FibCached() {
            fibsCached.add(0l); // #0
            fibsCached.add(1l); // #1
        }

        private ArrayList<Long> fibsCached = new ArrayList<>();
        private int fibsCompCount = 2;
        // in a correct caching implementation fibsCompCount will end up the same as fibsCached.size();

        public long getNthFib(int n) {
            if (fibsCached.size() > n) {
                //  If n is in the cach return it
                return fibsCached.get(n);
            } else {
                fibsCompCount++;
                long fibNum = getNthFib(n-1) + getNthFib(n-2);
                if (fibsCached.size() == n) {
                    fibsCached.add(fibNum);
                }
                return fibNum;
            }
        }

        /*
        // You can find implementation online, recursive or non-recursive.
        // For 100% solution you should use values in fibsCached as a starting point
        // instead of always starting from the first two values of 0, 1.
        private long computeNthFib(int n) {
            for (int i = fibsCached.size(); i < n; i++) {
                
            }
        }
        */

        // You are allowed to add another private method for fibonacci generation
        // if you want to use recursive approach. I recommend non-recursive though.
        // How many fibonacci numbers has your code computed as opposed to returned cached?
        // Use this in your testing to make sure your caching actually works properly.
        public int getCountOfFibsComputed() {
            return fibsCompCount;
        }
        
        @Override
        //  returns all cached Fib values, comma-space-separated
        public String toString() {
            /*  
            //  Wastes Storage
            String output = "[";
            for (int i = 0; i < fibsCached.size(); i++) {
                output += (i == 0 ? "" : ",") + fibsCached.get(i);
            }
            output += "]";
            return output;
            */
            StringBuffer sb = new StringBuffer();
            sb.append("[");
            for (int i = 0; i < fibsCached.size(); i++) {
                sb.append((i == 0 ? "" : ","));
                sb.append(fibsCached.get(i));
            }
            sb.append("]");
            return sb.toString();
        }
    }
