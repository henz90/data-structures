package day02cachingfibonacci;

public class Day02CachingFibonacci {

    public static void main(String[] args) {
        FibCached fc = new FibCached();
        for (int i = 0; i < 93; i++) {
            System.out.printf("%d: %d\n", i, fc.getNthFib(i));
        }
        System.out.println("CACHE: " + fc);
    }

}
