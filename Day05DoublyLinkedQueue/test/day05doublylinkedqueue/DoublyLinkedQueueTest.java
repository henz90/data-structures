package day05doublylinkedqueue;

import org.junit.Test;
import static org.junit.Assert.*;

public class DoublyLinkedQueueTest {
    
    public DoublyLinkedQueueTest() {
    }

    /**
     * Test of enqueue method, of class DoublyLinkedQueue.
     */
    @Test
    public void testEnqueue() {
        System.out.println("enqueue");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        instance.enqueue("One");
        instance.enqueue("Two");
        instance.enqueue("Three");
        instance.enqueue("Four");
        instance.enqueue("Five");
        assertEquals("[One,Two,Three,Four,Five]",instance.getListForwardAsString());
    }

    /**
     * Test of dequeue method, of class DoublyLinkedQueue.
     */
    @Test
    public void testDequeue() {
        System.out.println("dequeue");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        instance.enqueue("One");
        instance.enqueue("Two");
        instance.enqueue("Three");
        instance.enqueue("Four");
        instance.enqueue("Five");
        instance.dequeue();
        assertEquals("[Two,Three,Four,Five]",instance.getListForwardAsString());
    }
    
        /**
     * Test of dequeue method, of class DoublyLinkedQueue.
     */
    @Test
    public void testDequeueNoEnqueue() {
        System.out.println("dequeue");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        instance.dequeue();
        assertEquals("[]",instance.toString());
    }

    /**
     * Test of getSize method, of class DoublyLinkedQueue.
     */
     @Test
    public void testGetSize() {
        System.out.println("getSize");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
        instance.enqueue("One");
        instance.enqueue("Two");
        instance.enqueue("Three");
        instance.enqueue("Four");
        instance.enqueue("Five");
        expResult = 5;
        result = instance.getSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListForwardAsString method, of class DoublyLinkedQueue.
     */
    @Test
    public void testGetListForwardAsString() {
        System.out.println("getListForwardAsString");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        instance.enqueue("One");
        instance.enqueue("Two");
        instance.enqueue("Three");
        instance.enqueue("Four");
        instance.enqueue("Five");
        assertEquals("[One,Two,Three,Four,Five]",instance.getListForwardAsString());
    }

    /**
     * Test of getListReverseAsString method, of class DoublyLinkedQueue.
     */
    @Test
    public void testGetListReverseAsString() {
        System.out.println("getListReverseAsString");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        instance.enqueue("One");
        instance.enqueue("Two");
        instance.enqueue("Three");
        instance.enqueue("Four");
        instance.enqueue("Five");
        assertEquals("[Five,Four,Three,Two,One]",instance.getListReverseAsString());
    }

    /**
     * Test of toString method, of class DoublyLinkedQueue.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        DoublyLinkedQueue instance = new DoublyLinkedQueue();
        instance.enqueue("One");
        instance.enqueue("Two");
        instance.enqueue("Three");
        instance.enqueue("Four");
        instance.enqueue("Five");
        assertEquals("[One,Two,Three,Four,Five]",instance.toString());
    }
    
}
