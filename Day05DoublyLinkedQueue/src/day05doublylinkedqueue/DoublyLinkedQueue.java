package day05doublylinkedqueue;

public class DoublyLinkedQueue {

    private class Container {

        Container next, prev;
        String value;
    }

    private Container head, tail;
    private int size = 0;

    public void enqueue(String value) {
        Container newCont = new Container();
        newCont.value = value;
        if (size == 0) {
            head = newCont;
            tail = newCont;
        } else {
            Container previous = tail;
            tail = newCont;
            previous.next = newCont;
            newCont.prev = previous;
        }
        size++;
    }

    public String dequeue() {
        if (size == 0) {
            return null;
        } else {
            Container toDelete = head;
            Container temp = toDelete.next;
            head = temp;
            size--;
            return "Removed";
        }
    } // returns null if queue is empty

    public int getSize() {
        return size;
    }

    public String getListForwardAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Container current = head; current != null; current = current.next) {
            sb.append(current == head ? "" : ",");
            sb.append(current.value);
        }
        sb.append("]");
        return sb.toString();
    }
// following "next" pointers
// example returned value: "[Abc,Def,Xyz]"

public String getListReverseAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Container current = tail; current != null; current = current.prev) {
            sb.append(current == tail ? "" : ",");
            sb.append(current.value);
        }
        sb.append("]");
        return sb.toString();
    } // following "prev" pointers
    // example returned value: "[Xyz,Def,Abc]"

    // Optional - or it can just call getListForwardAsString();
    @Override
public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        //  Loops over all items in the linked list
        for (Container current = head; current != null; current = current.next) {
            sb.append(current == head ? "" : ","); //  Comma Sepperated
            sb.append(current.value);
        }
        sb.append("]");
        return sb.toString();
    } // needed for Unit Testing validation

}
