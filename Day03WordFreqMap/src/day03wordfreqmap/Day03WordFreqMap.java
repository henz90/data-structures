package day03wordfreqmap;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

class InvalidValueException extends Exception {

    InvalidValueException(String msg) {
        super(msg);
    }
}

public class Day03WordFreqMap {

    static HashMap<String, Integer> wordFreq = new HashMap<String, Integer>();

    static void readDataFromFile() {    //  Count is wrong? also whitespace is included
        try ( Scanner fileInput = new Scanner(new File("Bible.txt")).useDelimiter("[ \\s\\t\\n\\r\\\\.,:;0123456789-]")) {
            while (fileInput.hasNext()) {
                var word = fileInput.next();
                if (word.length() == 0) continue;
                if (wordFreq.containsKey(word)) {
                    int num = wordFreq.get(word);
                    num++;
                    wordFreq.put(word, num);
                } else {
                    wordFreq.put(word, 1);
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    static void sortTopTen() {
        long start = System.currentTimeMillis();
        //  Create a list of pairs of valiues from Key-Values pairs stored in HashMap
        List list = new ArrayList(wordFreq.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });
        System.out.println("TOP TEN WORDS:");
        for (int i = 0; i < 10; i++) {
            System.out.println(list.get(i));
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("TOP TEN WORDS Operation took " + time + "ms");
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        readDataFromFile();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        startTime = System.currentTimeMillis();
        System.out.println("File took " + duration + " seconds to load");
        System.out.println("");
        System.out.println("GOod- Frequency = " + wordFreq.get("God"));
        System.out.println("LORD - Frequency = " + wordFreq.get("LORD"));
        System.out.println("called - Frequency = " + wordFreq.get("called"));
        System.out.println("blessing - Frequency = " + wordFreq.get("blessing"));
        System.out.println("");
        sortTopTen();
        endTime = System.currentTimeMillis();
        duration = (endTime - startTime) / 1000.0;
        System.out.println("Operations took " + duration + " seconds to load");
    }
}
