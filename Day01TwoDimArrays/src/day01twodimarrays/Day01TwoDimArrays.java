package day01twodimarrays;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Day01TwoDimArrays {

    static Scanner input = new Scanner(System.in);

    static int inputInt() {
        while (true) {
            try {
                int value = input.nextInt();
                return value;
            } catch (InputMismatchException ex) {
                input.nextLine();
                System.out.println("Invalid input, run again.");
                //System.exit(0);
            } finally {
                input.nextLine();
            }
        }
    }

    static int sumAll(int[][] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        return sum;
    }

    static int sumRow(int[][] array, int row) {
        int sum = 0;
        for (int col = 0; col < array[row].length; col++) {
            sum += array[row][col];
        }
        return sum;
    }

    static int sumCol(int[][] array, int col) {
        int sum = 0;
        for (int row = 0; row < array.length; row++) {
            sum += array[row][col];
        }
        return sum;
    }

    static double standardDeviation(int[][] array) {
        double sum = 0.0, standardDeviation = 0.0;
        int itemsCount = array.length * array[0].length;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        double average = sum / itemsCount;
        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array[row].length; col++) {
                standardDeviation += Math.pow(array[row][col] - average, 2);
            }
        }
        return Math.sqrt(standardDeviation / itemsCount);
    }

    static void pairs(int[][] array) {
        for (int row1 = 0; row1 < array.length; row1++) {
            for (int col1 = 0; col1 < array[row1].length; col1++) {
                for (int row2 = row1; row2 < array.length; row2++) {
                    for (int col2 = (row1==row2 ? col1 : 0); col2 < array[row2].length; col2++) {
                        if (row1==row2 && col1==col2){
                            continue;
                        }
                        int sum = array[row1][col1] + array[row2][col2];
                        if (isPrime(sum)) {
                            System.out.printf("[%d,%d]:%d + [%d,%d]:%d = %d which is a prime number\n",
                                    row1, col1, array[row1][col1], row2, col2, array[row2][col2], sum);
                        }
                            
                        }
                    }
                }
            }
    }

    static boolean isPrime(int inputNumber) {
        if (inputNumber <= 1) {
            return false;
        }
        boolean isItPrime = true;
        for (int i = 2; i <= inputNumber / 2; i++) {
            if ((inputNumber % i) == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("Please enter Width for the array:");
        int arrayWidth = inputInt();
        System.out.println("Please enter Height for the array:");
        int arrayHeight = inputInt();
        if (arrayWidth < 1 || arrayHeight < 1) {
            System.out.println("Error: Please enter a number greater than 1");
            System.exit(1);
        }
        //  Generate random numbers 1-100 and assign them to each item of that array.
        int[][] array = new int[arrayHeight][arrayWidth];
        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array[row].length; col++) {
                Random rand = new Random();
                int min = -99;
                int max = 99;
                int randomNum = rand.nextInt(max - min + 1) + min;
                array[row][col] = randomNum;
            }
        }
        //  Print Array Contents
        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array[row].length; col++) {
                System.out.printf("%4d, ", array[row][col]);
            }
            System.out.println("");
        }
        int sumAll = sumAll(array);
        System.out.println("Sum of all number in the array is: " + sumAll);
        //  Sum of Rows
        for (int i = 0; i < array.length; i++) {
            System.out.println("Row " + i + " Sum is: " + sumRow(array, i));
        }
        //  Sum of Columns
        for (int i = 0; i < array[0].length; i++) {
            System.out.println("Column " + i + " Sum is: " + sumCol(array, i));
        }
        //  Standard Deviation
        System.out.println("Standard Deviation is: " + standardDeviation(array));
        
        //  Pairs and their Primes
        pairs(array);
    }
}
