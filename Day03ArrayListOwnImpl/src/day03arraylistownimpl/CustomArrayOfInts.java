package day03arraylistownimpl;

public class CustomArrayOfInts {

    private int[] data = new int[1]; // only grows by doubling size, never shrinks
    private int size = 0; // how many items do you really have

    public int size() {
        return size;
    }

    public void add(int value) {
        if (data.length > size) {
            data[size] = value;
            size++;
        } else {
            growStorage();
            add(value);
        }
    }

    private void growStorage() {
        int[] dataNew = new int[data.length * 2];
        System.arraycopy(data, 0, dataNew, 0, size);
        data = dataNew;
    }
    
// TODO
    public void deleteByIndex(int index) {
        
    }
    
// TODO
    public void deleteByValue(int value) {  // delete first value matching

    }

    public void insertValueAtIndex(int value, int index) {
        if (data.length == size) {
            growStorage();
        }
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        //  Move data by 1 to make space for inserted item
        for (int i = size-1; i >= index; i--) {
            data[i+1] = data[i];
        }
        
        data[index] = value;
        size++;
    }

    public void clear() {
        size = 0;
    }

    public int get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return data[index];
    }
    
// TODO
    public int[] getSlice(int startIdx, int length) {
        return data;
    }

    @Override
    public String toString() {// returns String similar to: [3, 5, 6, -23]
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append((i == 0 ? "" : ","));
            sb.append(data[i]);
        }
        sb.append("]");
        return sb.toString();
    }
}
