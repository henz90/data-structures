package day03arraylistownimpl;

public class Day03ArrayListOwnImpl {

    public static void main(String[] args) {
        CustomArrayOfInts caoi = new CustomArrayOfInts();
        caoi.add(2);
        caoi.add(10);
        caoi.add(-5);
        caoi.add(100);
        System.out.println(caoi.get(2));
        System.out.println(caoi);
        caoi.insertValueAtIndex(88, 2);
        System.out.println(caoi);
    }
}
