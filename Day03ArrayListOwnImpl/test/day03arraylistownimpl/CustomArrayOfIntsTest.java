package day03arraylistownimpl;

import org.junit.Test;
import static org.junit.Assert.*;

public class CustomArrayOfIntsTest {
    
    public CustomArrayOfIntsTest() {
    }
    
    /**
     * Test of size method, of class CustomArrayOfInts.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        CustomArrayOfInts instance = new CustomArrayOfInts();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class CustomArrayOfInts.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        int value = 10;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.add(value);
    }

    /**
     * Test of deleteByIndex method, of class CustomArrayOfInts.
     */
    @Test
    public void testDeleteByIndex() {
        System.out.println("deleteByIndex");
        int index = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.deleteByIndex(index);
    }

    /**
     * Test of deleteByValue method, of class CustomArrayOfInts.
     */
    @Test
    public void testDeleteByValue() {
        System.out.println("deleteByValue");
        int value = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.deleteByValue(value);
    }

    /**
     * Test of insertValueAtIndex method, of class CustomArrayOfInts.
     */
    @Test
    public void testInsertValueAtIndex() {
        System.out.println("insertValueAtIndex");
        int value = 0;
        int index = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.insertValueAtIndex(value, index);
    }

    /**
     * Test of clear method, of class CustomArrayOfInts.
     */
    @Test
    public void testClear() {
        System.out.println("clear");
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.clear();
    }

    /**
     * Test of get method, of class CustomArrayOfInts.
     */
    @Test (expected = IndexOutOfBoundsException.class)
    public void testGet() {
        System.out.println("get");
        int index = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        int expResult = 0;
        int result = instance.get(index);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSlice method, of class CustomArrayOfInts.
     */
    @Test
    public void testGetSlice() {
        System.out.println("getSlice");
        int startIdx = 0;
        int length = 0;
        CustomArrayOfInts instance = new CustomArrayOfInts();
        int[] expResult = null;
        int[] result = instance.getSlice(startIdx, length);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of toString method, of class CustomArrayOfInts.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CustomArrayOfInts instance = new CustomArrayOfInts();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
