package day03linkedlistarray;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.Timeout;

public class LinkedListArrayOfStringsTest {

    @Rule
    public Timeout globalTimeout = new Timeout(10 * 1000); //   Max time given for any text to excecute

    @Test(timeout = 5000)    //  5 seconds timeout for this particular test
    public void LinkedListArrayOfStringsAddMany() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        instance.add("Terry");
        instance.add("Barry");
        instance.add("Larry");
        assertEquals(4, instance.getSize());
        assertEquals("[Jerry,Terry,Barry,Larry]", instance.toString());
    }

    @Test
    public void LinkedListArrayOfStringsGetMany() {
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        instance.add("Terry");
        instance.add("Barry");
        instance.add("Larry");
        assertEquals("Larry", instance.get(3));
        assertEquals("Barry", instance.get(2));
        assertEquals("Jerry", instance.get(0));
        assertEquals("Terry", instance.get(1));
    }

    /**
     * Test of add method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        instance.add("Terry");
        instance.add("Barry");
        instance.add("Larry");
        assertEquals("Larry", instance.get(3));
        assertEquals("Barry", instance.get(2));
        assertEquals("Jerry", instance.get(0));
        assertEquals("Terry", instance.get(1));
    }

    /**
     * Test of get method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        int index = 0;
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        String expResult = "Jerry";
        String result = instance.get(index);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertValueAtIndex method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testInsertValueAtIndex() {
        System.out.println("insertValueAtIndex");
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        instance.add("Terry");
        instance.add("Barry");
        instance.add("Larry");
        instance.insertValueAtIndex(0, "Eva");
        assertEquals("[Eva,Jerry,Terry,Barry,Larry]", instance.toString());
        instance.insertValueAtIndex(2, "Tom");
        assertEquals("[Eva,Jerry,Tom,Terry,Barry,Larry]", instance.toString());
        instance.insertValueAtIndex(6, "Phil");
        assertEquals("[Eva,Jerry,Tom,Terry,Barry,Larry,Phil]", instance.toString());
    }

    /**
     * Test of deleteByIndex method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testDeleteByIndex() {
        System.out.println("deleteByIndex");
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        instance.add("Terry");
        instance.add("Barry");
        instance.add("Larry");
        instance.deleteByIndex(0);
        assertEquals("[Terry,Barry,Larry]", instance.toString());
        instance.deleteByIndex(2);
        assertEquals("[Terry,Barry]", instance.toString());
    }

    /**
     * Test of deleteByValue method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testDeleteByValue() {
        System.out.println("deleteByValue");
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        instance.add("Jerry");
        instance.add("Larry");
        instance.add("Terry");
        instance.add("Barry");
        instance.add("Larry");
        instance.deleteByValue("Larry");
        assertEquals("[Jerry,Terry,Barry,Larry]", instance.toString());
        instance.deleteByValue("Larry");
        assertEquals("[Jerry,Terry,Barry]", instance.toString());
        instance.deleteByValue("Garry");
        assertEquals("[Jerry,Terry,Barry]", instance.toString());
    }

    /**
     * Test of getSize method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toArray method, of class LinkedListArrayOfStrings.
     */
    @Test
    public void testToArray() {
        System.out.println("toArray");
        LinkedListArrayOfStrings instance = new LinkedListArrayOfStrings();
        String[] expResult = null;
        String[] result = instance.toArray();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
