package day03linkedlistarray;

public class LinkedListArrayOfStrings {

    private class Container {

        Container next;
        String value;
    }

    private Container start, end;
    private int size;

    public void add(String value) {
        Container newCont = new Container();
        newCont.value = value;
        if (size == 0) {
            start = newCont;
            end = newCont;
            size++;
        } else {
            end.next = newCont;
            end = newCont;
            size++;
        }
    }

    public String get(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            int count = 0;
            for (Container current = start; current != null; current = current.next) {
                if (index == count) {
                    return current.value;
                }
                count++;
            }
        }
        throw new RuntimeException("Invalid Control Flow");
    }

    public void insertValueAtIndex(int index, String value) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (size == 0 || index == size) {
            add(value);
            return;
        }
        if (index == 0) {
            Container newCont = new Container();
            newCont.value = value;
            newCont.next = start;
            start = newCont;
            size++;
            return;
        }
        Container newCont = new Container();
        int count = 0;
        for (Container current = start; current != null; current = current.next) {
            if (index - 1 == count) {
                Container old = current.next;
                current.next = newCont;
                newCont.next = old;
                newCont.value = value;
                size++;
                return;
            }
            count++;
        }

        throw new RuntimeException("Invalid Control Flow");
    }

    public void deleteByIndex(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (index == 0) {
            start = start.next;
            size--;
            return;
        }
        int count = 0;
        for (Container current = start; current != null; current = current.next) {
            if (index - 1 == count) {
                if (current.next == end) {
                    end = current;
                }
                current.next = current.next.next;
                size--;
                return;
            }
            count++;
        }
    }

    public boolean deleteByValue(String value) {
        int count = 0;
        for (Container current = start; current != null; current = current.next) {
            if (current.value.contains(value)) {
                this.deleteByIndex(count);
                return true;
            }
            count++;
        }
        return false;
    } // delete first value found

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        //  Loops over all items in the linked list
        for (Container current = start; current != null; current = current.next) {
            sb.append(current == start ? "" : ","); //  Comma Sepperated
            sb.append(current.value);
        }
        sb.append("]");
        return sb.toString();
    } // comma-separated values list similar to: [5,8,11]

    public String[] toArray() {
        String[] result = new String[size];
        int position = 0;
        for (Container current = start; current != null; current = current.next) {
            result[position] = current.value;
            position++;
        }
        return result;
    }
}
