
package day03arraysverified;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;


public class ArrayUtilsTest {
    
    public ArrayUtilsTest() {
    }
    
    @Before
    public void setUpClass() {
    }
    
    @After
    public void tearDownClass() {
    }

    /**
     * Test of sumAll method, of class ArrayUtils.
     */
    @Test
    public void testSumAllJagged() {
        System.out.println("sumAll");
        int[][] array = {{5, -1, 34}, {17, 2, 5, 5}, {22, 11, 3}};
        ArrayUtils instance = new ArrayUtils();
        int expResult = 103;
        int result = instance.sumAll(array);
        assertEquals("sumAll failed on Jagged Array", expResult, result);
    }
    
    @Test
    public void testSumAllRectangular() {
        System.out.println("sumAll");
        int[][] array = {{5, -1, 34}, {17, 2, 5}, {22, 11, 3}};
        ArrayUtils instance = new ArrayUtils();
        int expResult = 98;
        int result = instance.sumAll(array);
        assertEquals("sumAll failed on Jagged Array", expResult, result);
    }

    /**
     * Test of standardDeviation method, of class ArrayUtils.
     */
    @Test
    public void testStandardDeviationRectangular() {
        System.out.println("testStandardDeviationRectangular");
        int[][] array = {{5, -1, 34}, {17, 2, 5}, {22, 11, 3}};
        ArrayUtils instance = new ArrayUtils();
        double expResult = 10.785220797662d;
        double result = instance.standardDeviation(array);
        assertEquals("StandardDevbiation failed on Rectangular Array", expResult, result, 0.0000001);
    }
    
    @Test
    public void testStandardDeviationJagged() {
        System.out.println("testStandardDeviationJagged");
        int[][] array = {{5, -1, 34}, {17, 2, 5, 5}, {22, 11, 3}};
        ArrayUtils instance = new ArrayUtils();
        double expResult = 10.383159442097d;
        double result = instance.standardDeviation(array);
        assertEquals("StandardDevbiation failed on Jagged Array", expResult, result, 0.0000001);
    }
    
}
