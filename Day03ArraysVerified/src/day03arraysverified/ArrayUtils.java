package day03arraysverified;

public class ArrayUtils {

    public int sumAll(int[][] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        return sum;
    }

    public double standardDeviation(int[][] array) {
        double sum = 0.0, standardDeviation = 0.0;
        //int itemsCount = array.length * array[0].length;    //  Rectangular Arrays
        int itemsCount = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
                itemsCount++;
            }
        }
        double average = sum / itemsCount;
        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array[row].length; col++) {
                standardDeviation += Math.pow(array[row][col] - average, 2);
            }
        }
        return Math.sqrt(standardDeviation / itemsCount);
    }
}
