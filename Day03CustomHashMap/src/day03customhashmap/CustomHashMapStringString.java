package day03customhashmap;

import java.util.Arrays;

class CustomHashMapStringString {

    private class Container {

        Container next;
        String key;
        String value;
    }

    private Container[] hashTable = new Container[5];

    private int totalItems = 0;

    private int computeHash(String input) {
        int hash = 0;
        for (char c : input.toCharArray()) {
            hash = 31 * hash + (c & 0xff);
        }
        return hash;
    }

    /*      //  Gregory Hash 
    private int computeHash(String input) {
        int hash = 0;
        for (int i = 0; i < input.length(); i++) {
            hash <<= 1;
            char c = input.charAt(i);
            hash += c;
        }
        return hash;
    }
     */
    String getValue(String key) {
        int hash = computeHash(key);
        int index = hash % hashTable.length;
        for (Container current = hashTable[index]; current != null; current = current.next) {
            if (current.key.equals(key)) {
                return current.value;
            }
        }
        return null;
    }

    // LATER: expand hashTable by about *2 when totalItems > 3*hashTable.length
    void putValue(String key, String value) {
        int hash = computeHash(key);
        int index = hash % hashTable.length;
        //  Either find existing item in the list or create a new one.
        for (Container current = hashTable[index]; current != null; current = current.next) {
            if (current.key.equals(key)) {
                current.value = value;
                return;
            }
        }
        //  We only reach this code if key was not found
        Container newCont = new Container();
        newCont.key = key;
        newCont.value = value;
        newCont.next = hashTable[index];
        hashTable[index] = newCont;
        totalItems++;
    }

    boolean hasKey(String key) {
        int hash = computeHash(key);
        int index = hash % hashTable.length;
        for (Container current = hashTable[index]; current != null; current = current.next) {
            if (current.key.equals(key)) {
                return true;
            }
        }
        return false;
    }

    boolean deleteByKey(String key) {
        int hash = computeHash(key);
        int index = hash % hashTable.length;
        Container previous = null;
        Container current;
        for (current = hashTable[index]; current != null; current = current.next) {
            if (current.key.equals(key)) {
                break;
            }
            previous = current;
        }
        //  3 Cases:    1. Key not Found.   2. Removing First Entry 3. Removing a further entry
        if (current == null) {
            //  Reached end of list without finding Key
            return false;
        }
        if (previous == null) {
            //  Remove the 1st Entry
            hashTable[index] = hashTable[index].next;
            totalItems--;
            return true;
        } else {
            //  Remove the entry.
            previous.next = current.next.next;
            totalItems--;
            return true;
        }
    }

    public String[] getAllKeys() {
        String[] result = new String[totalItems];
        int nextResultIndexed = 0;
        for (int i = 0; i < hashTable.length; i++) {
            for (Container current = hashTable[i]; current != null; current = current.next) {
                result[nextResultIndexed] = current.key;
                nextResultIndexed++;
            }
        }
        return result;
    }

    int getSize() {
        return totalItems;
    }

    // print hashTable content, one entry per line, with all items in it.
    public void printDebug() {
        for (int i = 0; i < hashTable.length; i++) {
            System.out.printf("Entry %d\n", i);
            for (Container current = hashTable[i]; current != null; current = current.next) {
                System.out.printf(" - KEY %s, Value %s\n", current.key, current.value);
            }
        }
    }

    @Override   // e.g. [ Key1 => Val1, Key2 => Val2, ... ]
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        //  Values displayed in alphabetical order of keys
        String [] keysArray = getAllKeys();
        Arrays.sort(keysArray);
        for (int i = 0; i < keysArray.length; i++) {
            String key = keysArray[i];
            String value = getValue(key);
            sb.append(i==0 ? "" : ",");
            sb.append(key);
            sb.append("=>");
            sb.append(value);
        }
        sb.append("]");
        return sb.toString();
    } // comma-separated values->key pair list
}
