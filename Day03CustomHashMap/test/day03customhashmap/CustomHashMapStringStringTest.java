package day03customhashmap;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.Timeout;

public class CustomHashMapStringStringTest {
    
    public CustomHashMapStringStringTest() {
    }
    
    @Rule
    public Timeout globalTimeout = new Timeout(10*1000);

    /**
     * Test of getValue method, of class CustomHashMapStringString.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        String key = "";
        CustomHashMapStringString instance = new CustomHashMapStringString();
        String expResult = "";
        String result = instance.getValue(key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of putValue method, of class CustomHashMapStringString.
     */
    @Test
    public void testPutValue() {
        System.out.println("putValue");
        CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.putValue("Greg", "Red");
        instance.putValue("Phil", "Yellow");
        instance.putValue("Simon", "Blue");
        instance.putValue("Jerry", "Brown"); 
        assertEquals(4,instance.getSize());
        assertEquals("[Greg=>Red,Jerry=>Brown,Phil=>Yellow,Simon=>Blue]", instance.toString());
    }

    /**
     * Test of hasKey method, of class CustomHashMapStringString.
     */
    @Test
    public void testHasKey() {
        System.out.println("hasKey");
        String key = "";
        CustomHashMapStringString instance = new CustomHashMapStringString();
        boolean expResult = false;
        boolean result = instance.hasKey(key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteByKey method, of class CustomHashMapStringString.
     */
    @Test
    public void testDeleteByKey() {
        System.out.println("deleteByKey");
        CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.putValue("Greg", "Red");
        instance.putValue("Phil", "Yellow");
        instance.putValue("Simon", "Blue");
        instance.putValue("Jerry", "Brown");
        assertEquals(4,instance.getSize());
        assertEquals("[Greg=>Red,Jerry=>Brown,Phil=>Yellow,Simon=>Blue]", instance.toString());
        boolean result1 = instance.deleteByKey("Jimmy");
        assertEquals(false, result1);
        assertEquals(4,instance.getSize());
        boolean result2 = instance.deleteByKey("Jerry");
        assertEquals(true, result2);
        assertEquals(3,instance.getSize());
    }

    /**
     * Test of getAllKeys method, of class CustomHashMapStringString.
     */
    @Test
    public void testGetAllKeys() {
        System.out.println("getAllKeys");
        CustomHashMapStringString instance = new CustomHashMapStringString();
        String[] expResult = null;
        String[] result = instance.getAllKeys();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSize method, of class CustomHashMapStringString.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        CustomHashMapStringString instance = new CustomHashMapStringString();
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of printDebug method, of class CustomHashMapStringString.
     */
    @Test
    public void testPrintDebug() {
        System.out.println("printDebug");
        CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.printDebug();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class CustomHashMapStringString.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CustomHashMapStringString instance = new CustomHashMapStringString();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
